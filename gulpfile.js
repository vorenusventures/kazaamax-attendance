var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

let config = elixir.config;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix
    .copy('node_modules/font-awesome/fonts/**', 'public/fonts/')
    .copy('resources/assets/images/**', 'public/images/')
    .copy('resources/views/index.html', './public/index.html')
    .copy('resources/assets/js/data.json', './public/js/data.json')
    .scripts([
      './node_modules/toastr/build/toastr.css'
    ], './public/css/vendors.css')
    .sass('app.scss')
    .webpack('app.js', null, null, {
      exclude: [/joi-browser/]
    });

  mix.browserSync({
    server: ['public'],
    proxy: null,
    notify: false,
    open: false
  });
});
