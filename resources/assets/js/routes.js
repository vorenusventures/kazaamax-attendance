module.exports = [
  {
    name: 'home',
    path: '/',
    component: require('./components/Home.vue')
  },
  {
    name: 'settings',
    path: '/settings',
    component: require('./components/Settings.vue')
  },
  {
    name: 'timeline',
    path: '/timeline',
    component: require('./components/Timeline.vue')
  },
  {
    path: '/events/:eventId',
    component: require('./components/Event.vue'),
    children: [
      {
        name: 'event.show',
        path: '',
        component: require('./components/Attendees.vue')
      },
      {
        name: 'attendee.create',
        path: 'attendees/create',
        component: require('./components/Create-Attendee.vue')
      },
      {
        name: 'attendee.edit',
        path: 'attendees/:attendeeGUID/edit',
        component: require('./components/Edit-Attendee.vue')
      },
    ]
  },
  {
    path: '*',
    redirect: '/settings'
  }
];
