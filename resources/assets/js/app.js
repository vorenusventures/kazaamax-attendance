window.toastr = require('toastr');
window.lodash = window._ = require('lodash');
window.jQuery = window.$ = require('jquery');
window.Joi = require('joi-browser');
window.moment = require('moment');
window.toastr = require('toastr');

require('bootstrap-sass');

import Vue from "vue";
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import {initializeData} from "./vuex/actions";
import store from "./vuex/store";
import routes from "./routes";
import eventService from "./services/eventService";
import vueFilters from "./filters/filters";
import * as types from "./vuex/mutation-types";

window.Vue = Vue;
require('./spark-form/errors');
require('./spark-form/form');
require('./forms/components');

vueFilters.loadVueFilters();

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.component('navbar', require('./components/NavBar.vue'));

const router = new VueRouter({
  linkActiveClass: 'active',
  routes
});

router.afterEach((to, from) => {
  $('#nav').collapse('hide');
});

var localEvents = eventService.getEvents();
store.commit(types.SET_EVENTS, localEvents);
var app;
// I have to use jQuery here - Vue.http fails in iOS WebView
$.getJSON('js/data.json')
  .then(response => {
    //toastr.success('Loaded data.json successfully');
    //console.log('response', response);
    var data = response;
    if (eventService.syncRequired(data.synced_at)) {
      eventService.loadRemoteEvents(data.events, data.synced_at);
      toastr.success('Data updated from Mobile Locker');
    }
    initializeData(store, data);

    app = new Vue({
      el: '#app',
      router,
      store
    });
  })
  .catch(err => {
    toastr.error(err, 'Data Error');
    console.error('error', err);
  });
