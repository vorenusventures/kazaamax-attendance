import _ from 'lodash';
let ret = _.map(require('json!./countries.json'), c => {
  return {
    id: c.Code,
    name: c.Name,
  }
});

export default ret;
