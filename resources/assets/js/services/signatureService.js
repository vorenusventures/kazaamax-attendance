let $ = require('jquery');

module.exports = {
  initSignatureCapture(form) {
    console.log('initSignatureCapture()', form);
    let wrapper = $('.SignaturePad__Wrapper');
    let width = wrapper.innerWidth();
    let height = 100;
    $('#signaturePad').attr('width', width).attr('height', height);

    var canvas = new fabric.Canvas('signaturePad', {
      isDrawingMode: true,
      allowTouchScrolling: false
    });
    canvas.freeDrawingBrush = new fabric['PencilBrush'](canvas);
    canvas.freeDrawingBrush.color = '#000000';
    canvas.freeDrawingBrush.width = 5;
    canvas.on('mouse:up', function (options) {
      form.signatureJSON = JSON.stringify(canvas);
      form.signature = canvas.toDataURL({});
    });
    if (form.signatureJSON) {
      canvas.loadFromJSON(form.signatureJSON);
      canvas.trigger('mouse:up');
    }
    return canvas;
  },
  clearSignature(form, signatureCanvas) {
    form.signature = null;
    form.signatureJSON = null;
    signatureCanvas.clear();
  }
}
