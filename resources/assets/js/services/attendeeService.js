import moment from "moment";
import _ from "lodash";
import uuid from "uuid";
import eventService from "./eventService";
import capitalize from "capitalize";

export default {
  /**
   * Create a new Attendee object by copying values from the remote attendee object
   * @param remoteEvent
   * @param remoteAttendee
   * @returns {{id: *, guid: *, salutation: (null|*|Document.salutation), salutation_other: null, first_name: (null|*|Document.first_name), last_name: (null|*|Document.last_name), email: (null|*|string|string|Document.email), country: (*|null|Document.country), country_other: null, organization: (null|*|Document.organization), specialty: (null|*|Document.specialty), specialty_other: null, status: string, event_id: *, event_guid: *, source: *, signature: null, signed_at: null, created_at: (*|Moment|number), updated_at: (*|Moment|number)}}
   */
  createFromRemote(remoteEvent, remoteAttendee) {
    return {
      id: remoteAttendee.id,
      guid: remoteAttendee.guid,
      salutation: remoteAttendee.salutation,
      salutation_other: null,
      first_name: remoteAttendee.first_name,
      last_name: remoteAttendee.last_name,
      email: remoteAttendee.email,
      country: remoteAttendee.country,
      country_other: null,
      organization: remoteAttendee.organization,
      specialty: remoteAttendee.specialty,
      specialty_other: null,
      status: (remoteAttendee.signed_at) ? 'registered' : 'new',
      event_id: remoteEvent.id,
      event_guid: remoteEvent.guid,
      source: remoteAttendee.source,
      signature: (remoteAttendee.signature) ? remoteAttendee.signature : null,
      signatureJSON: (remoteAttendee.signatureJSON) ? remoteAttendee.signatureJSON : null,
      signed_at: (remoteAttendee.signed_at) ? remoteAttendee.signed_at : null,
      created_at: remoteAttendee.created_at,
      updated_at: remoteAttendee.updated_at
    }
  },
  newAttendee(event) {
    return {
      id: null,
      guid: null,
      salutation: null,
      salutation_other: null,
      first_name: null,
      last_name: null,
      email: null,
      country: null,
      country_other: null,
      organization: null,
      organization_other: null,
      specialty: null,
      specialty_other: null,
      signature: null,
      signatureJSON: null,
      status: 'Active',
      event_id: (event) ? event.id : null,
      event_guid: (event) ? event.guid : null,
      source: 'app',
      signed_at: null,
      created_at: moment().unix(),
      updated_at: moment().unix()
    }
  },
  saveAttendee(event, form) {
    let attendee = _.omit(form, ['errors', 'busy', 'successful']);

    if (!attendee.guid) {
      attendee.guid = uuid.v4();
    }

    if (!attendee.signed_at) {
      attendee.signed_here = true;
      attendee.signed_at = moment().unix();
    }

    // Capitalize the name fields
    if (attendee.first_name) {
      attendee.first_name = capitalize(attendee.first_name);
    }
    if (attendee.last_name) {
      attendee.last_name = capitalize(attendee.last_name);
    }
    attendee.name = `${attendee.first_name} ${attendee.last_name}`;

    if (attendee.email) {
      attendee.email = attendee.email.toLowerCase();
    }

    // Capitalize the "Other" fields
    if (attendee.country_other) {
      attendee.country_other = capitalize(attendee.country_other);
    }
    if (attendee.organization_other) {
      attendee.organization_other = capitalize(attendee.organization_other);
    }
    if (attendee.specialty_other) {
      attendee.specialty_other = capitalize(attendee.specialty_other);
    }

    // First try to find the Attendee by the GUID
    var index = -1;
    if (attendee.guid) {
      index = _.findIndex(event.attendees, function (a) {
        return (attendee.guid == a.guid);
      });
    }

    // Next try to find it by the ID
    if (index == -1 && attendee.id) {
      index = _.findIndex(event.attendees, function (a) {
        return ((a.id) && attendee.id == a.id);
      });
    }
    // If the attendee was created locally
    if (index == -1) {
      index = _.findIndex(event.attendees, function (a) {
        return (attendee.email == a.email);
      });
    }
    if (index == -1) {
      event.attendees.push(attendee);
    } else {
      event.attendees[index] = attendee;
    }
    //console.log(`saving attendee`, attendee);
    eventService.saveEvent(event);
  }
};
