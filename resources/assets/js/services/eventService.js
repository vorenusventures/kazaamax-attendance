import moment from "moment";
import _ from "lodash";
import store from "../vuex/store";
import * as types from "../vuex/mutation-types";
import attendeeService from "./attendeeService";
import mobilelockerService from "mobilelocker-tracking";
import capitalize from "capitalize";
export const SYNCED_AT_KEY = 'attendance_synced_at';
export const EVENTS_KEY = 'attendance_events';

export default {
  clearStorage() {
    localStorage.removeItem(EVENTS_KEY);
    localStorage.removeItem(SYNCED_AT_KEY);
    store.commit('SET_EVENTS', []);
  },
  saveEvents(events) {
    events.forEach(e => {
      if (_.isUndefined(e.current)) {
        e.current = false;
      }
      if (_.isArray(e.attendees)) {
        e.attendees.forEach(a => {
          if (_.isUndefined(a.name)) {
            a.name = `${capitalize(a.first_name)} ${capitalize(a.last_name)}`;
          }
          if (_.isUndefined(a.signature)) {
            a.signature = null;
          }
          if (_.isUndefined(a.signatureJSON)) {
            a.signatureJSON = null;
          }
        });
      }
    });
    //console.log(`saveEvents ${typeof events}`, events);
    localStorage.setItem(EVENTS_KEY, JSON.stringify(events));
    store.commit('SET_EVENTS', events);
  },
  getEvents() {
    try {
      if (!localStorage) {
        console.error(`The localStorage object does not exist`);
        return [];
      }
      if (localStorage.hasOwnProperty(EVENTS_KEY)) {
        let events = JSON.parse(localStorage.getItem(EVENTS_KEY));
        if (_.isArray(events)) {
          return events;
        }
        console.warn(`The ${EVENTS_KEY} was in localStorage but had type ${typeof events} instead of array.`);
      } else {
        console.warn(`localStorage does not have the "${EVENTS_KEY}" key.`);
      }
      return [];
    } catch (e) {
      console.error(`There was an error parsing the ${EVENTS_KEY} from localStorage`, e);
      return [];
    }
  },
  makeEventCurrent(event) {
    let events = this.getEvents();
    _.each(events, e => {
      if (e.id == event.id) {
        e.current = true;
      } else {
        e.current = false;
      }
    });
    this.saveEvents(events);
    store.commit(types.SET_EVENT_ID, event.id);
  },
  getCurrentEvent() {
    return _.find(this.getEvents(), e => {
      return e.current == true;
    });
  },
  getActiveEvents() {
    return _.filter(this.getEvents(), e => {
      return e.status == 'Active';
    });
  },
  getEventByID(eventID) {
    let events = this.getEvents();
    return _.find(events, e => {
      return e.id == eventID
    });
  },
  getEventByGUID(eventGUID) {
    let events = this.getEvents();
    return _.find(events, e => {
      return e.guid == eventGUID
    });
  },
  saveEvent(event) {
    event.date_name = `${event.start_date}_${event.name}`;
    event.professional = (event.company_role) ? false : true;
    event.active = (event.status == 'Active');

    var events = this.getEvents();
    var index = _.findIndex(events, function (e) {
      return e.id == event.id;
    });

    if (index == -1) {
      events.push(event);
    } else {
      events[index] = event;
    }

    this.saveEvents(events);
  },
  getSyncedAt() {
    if (localStorage.getItem(SYNCED_AT_KEY)) {
      return localStorage.getItem(SYNCED_AT_KEY);
    }
    console.warn(`${SYNCED_AT_KEY} was not set or was not a number so 0 is being returned.`);
    return 0;
  },
  syncRequired(syncedAt) {
    return this.getSyncedAt() < syncedAt;
  },
  loadRemoteEvents(remoteEvents, syncedAt) {
    let remoteEventIDs = [];
    _.forEach(remoteEvents, remoteEvent => {
      remoteEventIDs.push(remoteEvent.id);
      var event = this.getEventByGUID(remoteEvent.guid);
      if (event) {
        // Try to update every remote event
        if (1 == 1 || remoteEvent.updated_at > event.updated_at) {
          event.name = remoteEvent.name;
          event.status = remoteEvent.status;
          event.start_date = moment(remoteEvent.start_date).toDate();
          event.end_date = (remoteEvent.end_date) ? moment(remoteEvent.end_date).toDate() : null;
          event.venue = remoteEvent.venue;
          event.city = remoteEvent.city;
          event.country = remoteEvent.country;
          event.postal_code = remoteEvent.postal_code;
          event.category = remoteEvent.event_type_category;
          event.type = remoteEvent.event_type;
          event.source = remoteEvent.source;
          event.url = remoteEvent.url;
          event.focus = remoteEvent.focus;
          event.project_lead = remoteEvent.project_lead;
          event.company_role = remoteEvent.company_role;

          console.log(`Remote event ${event.id} has status: ${event.status}`, remoteEvent);
          _.forEach(remoteEvent.attendees, remoteAttendee => {
            var attendee = null;
            // Find the attendee
            var index = _.findIndex(event.attendees, function (a) {
              return (remoteAttendee.guid == a.guid);
            });

            if (index != -1) {
              attendee = event.attendees[index];

              // Update the attendee's information, only if it was updated on the server more recently than locally
              if (remoteAttendee.updated_at > attendee.updated_at) {
                attendee.salutation = remoteAttendee.salutation;
                attendee.first_name = remoteAttendee.first_name;
                attendee.last_name = remoteAttendee.last_name;
                attendee.email = remoteAttendee.email;
                attendee.country = remoteAttendee.country;
                attendee.organization = remoteAttendee.organization;
                attendee.specialty = remoteAttendee.specialty;
                attendee.created_at = remoteAttendee.created_at;
                attendee.updated_at = remoteAttendee.updated_at;
              }

              // The Attendee signed in on another iPad
              if (remoteAttendee.signed_at) {
                attendee.signed_at = remoteAttendee.signed_at;
                attendee.status = 'registered';
              }

              event.attendees[index] = attendee;
            } else {
              attendee = attendeeService.createFromRemote(remoteEvent, remoteAttendee);
              event.attendees.push(attendee);
            }
          });
          //console.log(`updating remote event ${event.id}`);
          this.saveEvent(event);
        }

      } else {
        // Create the event
        event = {
          id: remoteEvent.id,
          guid: remoteEvent.guid,
          name: remoteEvent.name,
          category: remoteEvent.event_type_category,
          type: remoteEvent.event_type,
          start_date: (remoteEvent.start_date) ? moment(remoteEvent.start_date).toDate() : null,
          end_date: (remoteEvent.end_date) ? moment(remoteEvent.end_date).toDate() : null,
          venue: remoteEvent.venue,
          city: remoteEvent.city,
          country: remoteEvent.country,
          attendees: [],
          status: remoteEvent.status,
          source: remoteEvent.source,
          url: remoteEvent.url,
          focus: remoteEvent.focus,
          project_lead: remoteEvent.project_lead,
          company_role: remoteEvent.company_role
        };

        _.forEach(remoteEvent.attendees, function (remoteAttendee) {
          var attendee = attendeeService.createFromRemote(remoteEvent, remoteAttendee);
          event.attendees.push(attendee);
        });
        //console.log(`saving new remote event ${event.id}`);
        this.saveEvent(event);
      }
    });

    // Any local event that didn't come from the remote feed will be deactivated.
    let localEvents = this.getEvents();
    _.forEach(localEvents, localEvent => {
      if (false == remoteEventIDs.includes(localEvent.id)) {
        localEvent.status = 'Inactive';
        this.saveEvent(localEvent);
      }
    });
    localStorage.setItem(SYNCED_AT_KEY, syncedAt);
  },
  uploadEvent(event) {
    console.log('uploading event', event);
    let form = _.omit(event, ['busy', 'errors', 'successful']);

    // Only submit attendees that have signatures
    /*form.attendees = _.filter(form.attendees, attendee => {
     return (attendee.signature) ? true : false;
     });*/

    return mobilelockerService.submitForm('attendance-form', form);
  }
};
