export const categories = state => {
  return state.categories;
};
export const countries = state => {
  return state.countries;
};
export const events = (state) => {
  return state.events;
};
export const organizations = state => {
  return state.organizations;
};
export const salutations = state => {
  return state.salutations;
};
export const specialties = state => {
  return state.specialties;
};
