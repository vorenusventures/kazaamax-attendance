import * as types from "./mutation-types";
import eventService from '../services/eventService';

export const loadEventsFromDataFile = (context) => {
  console.log('Loading data from js/data.json');
  return Vue.http.get('js/data.json')
    .then(response => {
      let data = response.data;
      console.log('Data loaded from file', data);
      // parse the data file and update the events in localStorage as necessary
      if (data.categories) {
        context.commit(types.SET_CATEGORIES, data.categories);
      }
      if (data.countries) {
        context.commit(types.SET_COUNTRIES, data.countries);
      }
      if (data.events) {
        context.commit(types.SET_EVENTS, data.events);
      }
      if (data.organizations) {
        context.commit(types.SET_ORGANIZATIONS, data.organizations);
      }
      if (data.salutations) {
        context.commit(types.SET_SALUTATIONS, data.salutations);
      }
      if (data.specialties) {
        context.commit(types.SET_SPECIALTIES, data.specialties);
      }
      return data;
    })
};

export const initializeData = (context, data) => {
  if (data.categories) {
    context.commit(types.SET_CATEGORIES, data.categories);
  }
  if (data.countries) {
    context.commit(types.SET_COUNTRIES, data.countries);
  }
  if (data.organizations) {
    context.commit(types.SET_ORGANIZATIONS, data.organizations);
  }
  if (data.salutations) {
    context.commit(types.SET_SALUTATIONS, data.salutations);
  }
  if (data.specialties) {
    context.commit(types.SET_SPECIALTIES, data.specialties);
  }
};
