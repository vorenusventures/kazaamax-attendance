import Vue from "vue";
import Vuex from "vuex";
import logger from "vuex/dist/logger";
import * as GETTERS from "./getters";
import * as ACTIONS from "./actions";

Vue.use(Vuex);
const state = {
  categories: [],
  countries: [],
  organizations: [],
  salutations: [],
  specialties: [],
  events: [],
  eventID: null,
};

const orderBy = function (items, field) {
  return lodash.orderBy(items, [item => item[field].toString().toLowerCase()]);
};

const mutations = {
  SET_EVENTS(state, events) {
    state.events = events;
  },
  SET_EVENT_ID(state, eventID) {
    state.eventID = eventID;
  },
  SET_CATEGORIES(state, categories) {
    categories.forEach(c => {
      c.id = c.code;
    });
    state.categories = orderBy(categories, 'name');
  },
  SET_COUNTRIES(state, countries) {
    countries.forEach(c => {
      c.id = c.code;
    });
    //countries.forEach(c => { c.name = capitalize(c.name)});
    state.countries = orderBy(countries.filter(c => {
      return c.code != 'Other'
    }), 'name');
    state.countries.push({name: 'Other', code: 'Other', id: 'Other'});
  },
  SET_ORGANIZATIONS(state, organizations) {
    organizations.forEach(c => {
      c.id = c.code;
    });
    state.organizations = orderBy(organizations.filter(o => {
      return o.code != 'Other'
    }), 'name');
    state.organizations.push({name: 'Other', code: 'Other', id: 'Other'});
  },
  SET_SALUTATIONS(state, salutations) {
    salutations.forEach(c => {
      c.id = c.code;
    });
    state.salutations = orderBy(salutations.filter(s => {
      return s.code != 'Other'
    }), 'name');
  },
  SET_SPECIALTIES(state, specialties) {
    specialties.forEach(c => {
      c.id = c.code;
    });
    state.specialties = orderBy(specialties.filter(s => {
      return s.code != 'Other'
    }), 'name');
    state.specialties.push({name: 'Other', code: 'Other', id: 'Other'});
  }
};

const debug = process.env.NODE_ENV !== 'production';

// See https://github.com/vuejs/vuex/blob/dev/examples/shopping-cart/store/index.js
export default new Vuex.Store({
  actions: ACTIONS,
  getters: GETTERS,
  mutations: mutations,
  state: state,
  strict: debug,
  plugins: debug ? [logger] : []
});
