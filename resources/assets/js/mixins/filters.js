import _ from 'lodash';
import capitalize from 'capitalize';
import accounting from 'accounting';
import pluralize from 'pluralize';

const orderBy = function (items, field, dir = 'asc') {
  return _.orderBy(items, [item => (item[field]) ? item[field].toString().toLowerCase() : null], [dir]);
};

const cap = (string) => {
  if (string == null) {
    return null;
  }
  return capitalize(string);
};

const capWords = (string) => {
  if (string == null) {
    return null;
  }
  return capitalize.words(string);
}

module.exports = {
  methods: {
    pluralize: pluralize,
    accounting: accounting,
    formatMoney: accounting.formatMoney,
    formatNumber: accounting.formatNumber,
    capitalize: cap,
    capitalizeWords: capWords,
    orderBy: orderBy,
  }
}
