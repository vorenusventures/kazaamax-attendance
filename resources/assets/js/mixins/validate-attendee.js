var Joi = require('joi');

module.exports = {
  methods: {
    validateAttendee(form, event, attendee = null) {
      let attendeeEmails = event.attendees.map(a => {
        return a.email;
      });
      if (attendee) {
        attendeeEmails = attendeeEmails.filter((e) => {
          return e != attendee.email
        });
      }
      //console.log('attendee emails', attendeeEmails);
      var schema = Joi.object().keys({
        salutation: Joi.string().required().label('Salutation'),
        first_name: Joi.string().label('First name'),
        last_name: Joi.string().label('Surname'),
        email: Joi.string().email().label('Email address').invalid(attendeeEmails).options({
          language: {
            any: {
              invalid: '!!This email address is already registered.'
            },
            string: {
              email: '!!Enter a valid email address.'
            }
          }
        }),
        country: Joi.string().label('Country'),
        country_other: Joi.when('country', {is: 'Other', then: Joi.string().label('Country')}),
        organization: Joi.string().label('Organization'),
        organization_other: Joi.when('organization', {is: 'Other', then: Joi.string().label('Organization')}),
        specialty: Joi.string().label('Specialty'),
        specialty_other: Joi.when('specialty', {is: 'Other', then: Joi.string().label('Specialty')}),
        signature: Joi.string().label('Signature').options({language: {any: {required: '!!Your signature is required.'}}})
      });

      var value = {
        salutation: form.salutation,
        first_name: form.first_name,
        last_name: form.last_name,
        email: form.email,
        country: form.country,
        country_other: form.country_other,
        organization: form.organization,
        organization_other: form.organization_other,
        specialty: form.specialty,
        specialty_other: form.specialty_other,
        signature: form.signature
      };
      let options = {
        abortEarly: false,
        allowUnknown: true,
        language: {
          string: {
            base: '{{key}} is required.'
          }
        }
      };
      return Joi.validate(value, schema, options);
    }
  }
};
