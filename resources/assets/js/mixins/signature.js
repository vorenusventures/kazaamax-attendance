import signatureService from '../services/signatureService';

module.exports = {
  data() {
    return {
      signatureCanvas: null
    }
  },
  methods: {
    initSignatureCapture: signatureService.initSignatureCapture,
    clearSignature: signatureService.clearSignature
  }
}
